package it.uniroma3.sensore;

import java.util.ArrayList;
import java.util.List;

import it.uniroma3.supporto.Costanti;

public class Controller {
	
	private double std_rumore; //dev. std. del rumore
	private List<Sensore> sensori; // lista di sensori da cui il controller riceve i dati
	private double[][] pd; // mappa che contiene le pd rilevate da ogni sensore

	public Controller(double std_rumore) {
		this.std_rumore = std_rumore;
		this.sensori = new ArrayList<>(Costanti.N_SENSORI);
		
		//cambiato
		this.pd = new double[Costanti.N_SENSORI][Costanti.N_SENSORI];
		creaSensori();
	}
	
	public double[][] getPd() {
		return this.pd;
	}

	/**
	 * banale metodo per istanziare gli oggetti sensore
	 */
	private void creaSensori() {
		for(int i=0 ; i<Costanti.N_SENSORI; i++)
		{
			this.sensori.add(new Sensore(this.std_rumore));
		}
	}
	/**
	 * metodo che raccoglie le probabilit� di detection dai sensori secondo la Semianalitica
	 * i for nidificati gesticono i sensori in modo da coinvolgere prima 1 sensore, poi 2,3...fino a 9
	 */
	public void raccogliPdSemianalitica() {
		for(int i = 0; i< Costanti.N_SENSORI; i++) {

			for(int j = 0 ; j < (i+1) ; j++){

				Sensore s = this.sensori.get(j);
				pd[i][j] = s.getPdSemianalitica();
				
			}
		}

	}
	/**
	 * metodo che raccoglie le probabilit� di detection dai sensori secondo la Teorica
	 * i for nidificati gesticono i sensori in modo da coinvolgere prima 1 sensore, poi 2,3...fino a 9
	 */
	public void raccogliPdTeorica() {
		for(int i = 0; i < Costanti.N_SENSORI ; i++) {

			for(int j = 0 ; j < (i+1); j++)
			{
				Sensore s = this.sensori.get(j);
				pd[i][j] = s.getPdTeorica();
			}
		}
	}

	/**
	 * metodo che raccoglie le probabilit� di detection dai sensori secondo la Simulata
	 */
	public void raccogliPdSimulata() {
		for(int i=0; i<Costanti.N_SENSORI ; i++) {

			for(int j = 0 ; j <(i + 1) ; j++)
			{
				Sensore s = new Sensore(this.std_rumore);
				pd[i][j] = s.getPdSimulata();
				//System.out.println("pd["+i+"]["+j+"]="+pd[i][j]);
			}
		}
	}
	
	/**
	 * metodo che decide se il primary user � presente con la regola dell'and 
	 * sulla base dei dati raccolti
	 */
	public boolean[] decidiConAnd() {
		boolean[] detection = new boolean[9];
		int i,j;
		for (i = 0; i<detection.length ; i++ ) {
			detection[i]=true;
		}

		for(i = 0; i<Costanti.N_SENSORI ;i++) {

			for(j=0 ; j < (i+1) && detection[i]; j++)
			{
				if(this.pd[i][j]<Costanti.PD_SOGLIA)
					detection[i] = false;
			}
		}
		return detection;
	}
	/**
	 * metodo che decide se il primary user � presente con la regola dell'or 
	 * sulla base dei dati raccolti
	 */
	public boolean[] decidiConOr() {
		boolean[] detection = new boolean[Costanti.N_SENSORI];
		int i,j;
		
		for(i = 0; i<detection.length; i++) {
			detection [i] =false;
		}
		
		for (i = 0 ; i< Costanti.N_SENSORI; i++ ) {

			for(j=0 ; j < (i+1) && !detection[i]; j++)
			{
				if(this.pd[i][j]>=(Costanti.PD_SOGLIA))
					detection[i] = true;
			}
		}
		return detection;
	}
	/**
	 * metodo che decide se il primary user � presente con la regola della maggioranza
	 * sulla base dei dati raccolti
	 */
	public boolean[] decidiConMaggioranza() {
		int i,j,count =0;
		boolean[] detection = new boolean[9];
		for( i = 0; i<Costanti.N_SENSORI ; i++) {

			for(j=0 ; j < (i+1) ; j++)
			{
				if(this.pd[i][j]>=(Costanti.PD_SOGLIA))
					count ++;
			}

			detection[i] = count>(Costanti.N_SENSORI/2);
		}

		return detection;
	}

}
