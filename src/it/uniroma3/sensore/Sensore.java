package it.uniroma3.sensore;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.complex.Complex;

import it.uniroma3.supporto.*;
/**
 * classe che rappresenta un sensore che rileva un segnale e calcola la probabilit� di detection
 * @author Alessandro
 *
 */
public class Sensore {
	
	private double std_rumore; //dev. std. del rumore
	private Complex[] segnale_pu; //segnale primary key user. modulato QPSK
	
	public Sensore(double std_rumore) {
		this.std_rumore = std_rumore;
		this.segnale_pu = GeneratoreSegnali.generaSegnalePU();	
	}
	
	/**
	 * calcolo energia di un segnale complesso
	 * @param segnale
	 * @return energia del segnale
	 */
	public double calcolaEnergia(Complex[] segnale) {
		double energia = 0;
		for(int i = 0 ; i<Costanti.N_CAMPIONI ; i++)
		{
			Complex si = segnale[i];
			Complex abs = si.multiply(si.conjugate());
			energia += abs.getReal();
		}
		return (double)energia/Costanti.N_CAMPIONI;
	}
	/**
	 * generazione array di energie nel caso H0 ad un determinato snr
	 * @param index_snr
	 * @return energie nel caso H0
	 */
	public double[] energieH0() {
		double energie[] = new double[Costanti.N_prove_H0];
		for(int i=0 ; i<Costanti.N_prove_H0; i++)
		{
			//System.out.println("std_rumore["+index_snr+"]="+this.std_rumore[index_snr]);
			Complex[] rumore = GeneratoreSegnali.generaRumore(this.std_rumore);
			energie[i] = calcolaEnergia(rumore);
			/* queste energie vengono tutte uguali... non capisco perch�*/
			//System.out.println("energia["+i+"]="+energie[i]);
		}
		return energie;
	}

	/**generazione array di energie nel cado H1 ad un determinato snr
	 * @param index_snr
	 * @return energie nel caso H1
	 */
	public double[] energieH1() {
		double energie[] = new double[Costanti.N_prove_H1];
		Complex [] segnaleRicevuto;
		for (int i = 0; i< Costanti.N_prove_H1;i++) {
			Complex [] rumore = GeneratoreSegnali.generaRumore(this.std_rumore);
			segnaleRicevuto = sommaVettoriComplex(rumore,this.segnale_pu);
			energie[i] = calcolaEnergia(segnaleRicevuto);
			//System.out.println("energia["+i+"]="+energie[i]);

		}
		return energie;
	}
	private Complex[] sommaVettoriComplex(Complex[] v1, Complex[] v2) {
		Complex [] segnale = new Complex [Costanti.N_CAMPIONI];
		for (int i = 0 ; i< Costanti.N_CAMPIONI;i++)
		{

			segnale[i] = v1[i].add(v2[i]);

		}
		return segnale;
	}

	/**
	 * cacolo della soglia per via teorica, in base al vettore di energie calcolato
	 * @param energieH0
	 * @return soglia teorica
	 */
	public double calcolaSogliaTeorica(double[] energieH0) {
		double soglia;
		double media = FunzioniMamatiche.mean(energieH0); //semrpe 0
		double varianza = FunzioniMamatiche.var(energieH0); // sempre 0
		soglia = media +(double) Math.sqrt((double)2*varianza) * FunzioniMamatiche.erfinv((double)1-(double)2*Costanti.PFA);
		//System.out.println("soglia = "+soglia);
		return soglia;
	}
	/**
	 * cacolo della soglia per via simulata, in base al vettore di energie calcolato
	 * @param energieH0
	 * @return soglia simulata
	 */
	public double calcolaSogliaSimulazione(double[] energieH0) {
		double soglia;
		List<Double> collection_energie = createSortedCollection(energieH0);
		int indice =(int) (Costanti.N_prove_H0 *Costanti.PFA);
		soglia = collection_energie.get(Costanti.N_prove_H0-indice);
		return soglia;
	}
	private List<Double> createSortedCollection(double[] energieH0) {
		List<Double> collection_energie = new ArrayList<>(Costanti.N_prove_H0);
		for(int i=0; i<Costanti.N_prove_H0; i++)
		{
			collection_energie.add(energieH0[i]);
		}
		collection_energie.sort(new ComparatoreDoulbe());
		return collection_energie;
	}
	/**
	 * calcolo Pd(probabilit� di detection) per via teorica
	 * @param soglia_teor
	 * @param energieH1
	 * @return probability of detection
	 */
	public double calcoloPdTeorica(double soglia_teor, double[] energieH1) {
		double media = FunzioniMamatiche.mean(energieH1);
		double var = FunzioniMamatiche.var(energieH1);
		double app = (double) 1/(Math.sqrt((double) 2*var));
		return 0.5 + (double)0.5*FunzioniMamatiche.erf( (double)(media - (double)soglia_teor)*app);
	}
	/**
	 * calcolo Pd per via simulata o seminalitica(se viene passata la soglia teorica)
	 * @param soglia
	 * @param energiaH1
	 * @return probabiliy of detection
	 */
	public double calcoloPdSimulata(double soglia, double[] energiaH1) {
		int count = 0;
		for(int i=0; i< energiaH1.length ;i++)
		{
			count += (energiaH1[i]>soglia) ? 1:0;
		}
		return (double)count/energiaH1.length;
	}
	
	public double getPdTeorica() {
		double sogliaTeorica = this.calcolaSogliaTeorica(this.energieH0());
		return this.calcoloPdTeorica(sogliaTeorica, this.energieH1());
	}
	public double getPdSimulata() {
		return this.calcoloPdSimulata(this.calcolaSogliaSimulazione(this.energieH0()), this.energieH1());
	}
	public double getPdSemianalitica() {
		return this.calcoloPdSimulata(this.calcolaSogliaTeorica(this.energieH0()), this.energieH1());
	}
	
}
