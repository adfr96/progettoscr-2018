package it.uniroma3.main;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Simulazione simulazione = new Simulazione();
		int x;
		Scanner inputScanner;

		System.out.println("\nScegli la simulazione da svolgere:");
		System.out.println("1) Test di spectrum sensing cooperativo per via Simulata");
		System.out.println("2) Test di spectrum sensing cooperativo per via Teorica");
		System.out.println("3) Test di spectrum sensing cooperativo per via Semianalitica");
		System.out.println("4) Test di spectrum sensing singolo per via teorica(con grafico)");
		System.out.println("5) Test di spectrum sensing singolo per via simulata(con grafico)");
		System.out.println("6) Test di spectrum sensing singolo per via semianalitica(con grafico)");
		System.out.println("0) chiudi applicazione");

		inputScanner = new Scanner(System.in);
		x = inputScanner.nextInt();
		inputScanner.close();
		switch (x) {
		case 1:
			simulazione.simulaPdSimulata();
			break;
		case 2:
			simulazione.simulaPdTeorica();
			break;
		case 3:
			simulazione.simulaSemianalitica();
			break;
		case 4:
			simulazione.testPdTeorica();
			System.out.println("Chiudere il grafico per interromepere l'applicazione");
			break;
		case 5:
			simulazione.testPdSimulata();
			System.out.println("Chiudere il grafico per interromepere l'applicazione");
			break;
		case 6:
			simulazione.testPdSemianalitica();
			System.out.println("Chiudere il grafico per interromepere l'applicazione");
			break;
		case 0:
			System.out.println("Arrivederci");
			break;
		default:
			System.out.println("Errore nell'inserimento.... ricarica l'applicazione");
			break;
		}

	}
}
