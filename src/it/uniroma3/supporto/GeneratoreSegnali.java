package it.uniroma3.supporto;

import java.util.Random;

import org.apache.commons.math3.complex.Complex;
/**
 * classe di supporto per generare i segnali per la simulazione
 * @author Alessandro
 *
 */
public class GeneratoreSegnali {
	
	/**
	 * metodo per generare il segnale del primary user, modulato QPSK
	 * @return array di Complex che rappresenta il segnale
	 */
	public static Complex[] generaSegnalePU() {
		Random rnd= new Random();
		Complex[] segnale_pu = new Complex[Costanti.N_CAMPIONI];
		for(int i=0; i<Costanti.N_CAMPIONI ; i++)
		{
			
			segnale_pu[i] = new Complex(Math.signum(0.5 - rnd.nextDouble()), Math.signum(0.5 - rnd.nextDouble()));
		}
		/*
		 * rendo il segnale a potenza 1
		 */
		
		double std = FunzioniMamatiche.std(segnale_pu);
		for(int i=0; i<Costanti.N_CAMPIONI ; i++)
		{
			segnale_pu[i] = segnale_pu[i].divide(std);
		}
		 
		return segnale_pu;
	}
	
	/**
	 * metodo per generare un rumore con distribuzione gaussiana
	 * @return un array[N_campioni] di Complex che rappresenta il rumore
	 */
	public static Complex[] generaRumore(double std_rumore) {
		Complex rumore[] = new Complex[Costanti.N_CAMPIONI];
		Random rnd = new Random();

		for(int i=0; i<Costanti.N_CAMPIONI ; i++)
		{
			rumore[i] = new Complex(rnd.nextGaussian(), rnd.nextGaussian());
		}
		/*
		 * rendo il rumore a potenza 1 e con la deviazione standard richesta
		 */
		double std = FunzioniMamatiche.std(rumore);
		for(int i=0; i<Costanti.N_CAMPIONI ; i++)
		{
			/*
			 * tolto il rendere il rumore a potenza uno, altrimenti vengono tutti con la stessa energia
			 */
			rumore[i] = rumore[i].divide(std);
			rumore[i] = rumore[i].multiply(std_rumore);	
		}
		//System.out.println("\n std rumore="+FunzioniMamatiche.std(rumore));
		return rumore;
	}
}
