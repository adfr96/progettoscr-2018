package it.uniroma3.supporto;

import org.apache.commons.math3.complex.Complex;
/**
 * classe che implementa le funzioni usate per la simulazione
 * @author Alessandro
 *
 */
public final class FunzioniMamatiche {
	/**
	 * funzione che calcola la media di un array di double
	 * @param c
	 * @return media di c
	 */
	public static final double mean(double[] c) {
		double somma = 0;
		for(int i=0 ; i<c.length ; i++)
		{
			somma += c[i];
		}
		return somma*((double)1/c.length);
	}
	/**
	 * funzione che calcola la deviazione standard di un array di double
	 * @param c
	 * @return std di c
	 */
	public static final double std(double[] c) {
		return Math.sqrt(var(c));
	}
	/**
	 * funzione che calcola la medi di un array di Complex
	 * @param c
	 * @return media di c
	 */
	public static final Complex mean(Complex[] c) {
		Complex somma = new Complex(0);
		for(int i=0 ; i<c.length ; i++)
		{
			somma = somma.add(c[i]);
		}
		somma = somma.multiply((double)1/c.length);
		return somma;
	}
	/**
	 * funzione che calcola la deviazione standard di un array di Complex
	 * @param c
	 * @return std di c
	 */
	public static final double std(Complex[] c) {
		return Math.sqrt(var(c));
	}
	/**
	 * funzione per calcolare la varianza di un array di double
	 * @param c
	 * @return varianza di cs
	 */
	public static final double var(double[] c) {
		double var,somma = 0;
		double mean =  mean(c);
		for(int i=0 ; i<c.length ; i++)
		{
			double app = c[i] - mean;
			app = app * app;
			somma += app;
		}
		var = ((double)1/(c.length-1)*somma);
		return var;
	}
	/**
	 * funzione per calcolare la varianza di un array di Complex
	 * @param c
	 * @return varianza di cs
	 */
	public static final double var(Complex[] c) {
		double var,somma = 0;
		Complex mean =  mean(c);
		//System.out.println("media di c"+ mean);
		for(int i=0 ; i<c.length ;i++)
		{
			Complex app = c[i].subtract(mean);
			app = app.multiply(app.conjugate());// modulo quadro
			somma += app.getReal(); // app � sicuramente un reale
			
		}
		var = ((double)1/(c.length-1))*somma;
		return var;
	}
	/**
	 * funzione erf(x) di matlab, riscritta in java discetizzando l'integrale
	 * @param x
	 * @return erf(x)
	 */
	public static final double erf(double x) {
		double erf=0;
		double delta=0.0001;
		for(double i=delta,j=0; i<x; i+=delta, j+=delta)
		{
			// sorteggio un numero compreso tra j ed i
			double e = (Math.random()*delta)+j;
			// cacolo la funione in quel punto
			double fe= Math.pow(Math.E, -(e*e));
			erf += fe * delta;
		}
		erf = ((double)2/Math.sqrt(Math.PI))*erf;
		return erf;
	}
	/**
	 * funzione erfinv di matlab, implementazione trovata in rete ---- FUNZIONA ---- 
	 * @param x
	 * @return erfinv(x)
	 */
	public static final double erfinv(double x) {
		double _a = ((8*(Math.PI - 3)) / ((3*Math.PI)*(4 - Math.PI)));
		double signX = ((x < 0) ? -1.0 : 1.0 );

		double oneMinusXsquared = 1.0 - (x * x);
		double LNof1minusXsqrd  = Math.log( oneMinusXsquared );
		double PI_times_a       = Math.PI * _a ;

		double firstTerm  = Math.pow(((2.0 / PI_times_a) + (LNof1minusXsqrd / 2.0)), 2);
		double secondTerm = (LNof1minusXsqrd / _a);
		double thirdTerm  = ((2 / PI_times_a) + (LNof1minusXsqrd / 2.0));

		double primaryComp = Math.sqrt( Math.sqrt( firstTerm - secondTerm ) - thirdTerm );

		double erfinv = signX * primaryComp ;
		return erfinv ;
	}
}
